package com.ftd.messagelistener.fragment;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.activity.AlarmActivity;
import com.ftd.messagelistener.activity.AlarmConfigActivity;
import com.ftd.messagelistener.activity.InterceptorActivity;
import com.ftd.messagelistener.activity.PermisssionActivity;
import com.ftd.messagelistener.bean.SMSBean;
import com.ftd.messagelistener.data.AppDatabase;
import com.ftd.messagelistener.receiver.AlarmReceiver;
import com.ftd.messagelistener.receiver.SMSReceiver;
import com.ftd.messagelistener.service.SMSService;
import com.ftd.messagelistener.utils.DateUtil;

import java.util.Calendar;
import java.util.concurrent.Executors;

import static android.content.Context.ALARM_SERVICE;

/**
 * tangxianfeng
 * 2021.11.28
 */
public class ConfigFragment extends Fragment {

    private View mLayout;
    public ConfigFragment() {
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        mLayout=inflater.inflate(R.layout.fragment_config, container, false);
        initView();
        return mLayout;
    }

    private void initView(){
        CardView Naozhon=mLayout.findViewById(R.id.config_testnaozhong);
        CardView interceptor=mLayout.findViewById(R.id.config_interceptor);
        CardView permission=mLayout.findViewById(R.id.config_permission);
        Naozhon.setOnClickListener(view -> startActivity(new Intent(getActivity(), AlarmConfigActivity.class)));
        interceptor.setOnClickListener(view -> startActivity(new Intent(getActivity(), InterceptorActivity.class)));
        permission.setOnClickListener(view -> startActivity(new Intent(getActivity(), PermisssionActivity.class)));
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}