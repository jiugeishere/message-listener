package com.ftd.messagelistener.fragment;

import android.os.Bundle;

import androidx.appcompat.widget.AppCompatButton;
import androidx.appcompat.widget.AppCompatEditText;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.adapter.MessageAdapter;
import com.ftd.messagelistener.bean.SMSBean;
import com.ftd.messagelistener.data.AppDatabase;
import com.ftd.messagelistener.utils.AndroidScheduler;
import com.ftd.messagelistener.utils.DateUtil;

import org.reactivestreams.Subscription;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

import io.reactivex.FlowableSubscriber;
import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

/**
 * tangxianfeng
 * 2021.11.28
 */
public class MessageListFragment extends Fragment {

    private static final String TAG="MessageListFragment";
    private MessageAdapter messageAdapter;
    private List<SMSBean> smsBeanList=new ArrayList<>();

    private RecyclerView recyclerView;
    private AppCompatImageView NodataImg;//无数据展示图
    private AppCompatButton button_search;
    private AppCompatEditText edit_search;
    public MessageListFragment() {
    }

    @Override
    public void onStart() {
        super.onStart();
        initView();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view=inflater.inflate(R.layout.fragment_message_list, container, false);
        recyclerView=view.findViewById(R.id.message_recy);
        NodataImg=view.findViewById(R.id.nodata_img);
        button_search=view.findViewById(R.id.button_search);
        edit_search=view.findViewById(R.id.edit_search);
        return view;
    }

    private void initView(){
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        button_search.setOnClickListener(view -> {
            AppDatabase.getInstance(getContext())
                    .smsdao()
                    .findBySource(edit_search.getText().toString())
                    .subscribeOn(Schedulers.newThread())
                    .observeOn(AndroidScheduler.mainThread())
                    .subscribe(new Observer<List<SMSBean>>() {
                        @Override
                        public void onSubscribe(@NonNull Disposable d) {
                            Log.e(TAG,"onSubscribe");

                        }

                        @Override
                        public void onNext(@NonNull List<SMSBean> list) {
                            Log.e(TAG,"onNext"+list.size());
                            refeshdata(list);
                        }

                        @Override
                        public void onError(@NonNull Throwable e) {

                        }

                        @Override
                        public void onComplete() {

                        }
                    });


        });
        //smsBeanList= SMSUtils.getSMS(getContext());
        if (messageAdapter==null){
            messageAdapter=new MessageAdapter(getContext(),smsBeanList);
        }
        recyclerView.setAdapter(messageAdapter);

       AppDatabase.getInstance(getContext())
                .smsdao()
                .getAll()
                .observe(this, this::refeshdata);

//                .subscribeOn(Schedulers.newThread())
//                .observeOn(AndroidScheduler.mainThread())
//                .subscribe(new FlowableSubscriber<List<SMSBean>>() {
//
//                    @Override
//                    public void onSubscribe(@NonNull Subscription s) {
//                        Log.e(TAG,"onSubscribe");
//                    }
//
//                   @Override
//                   public void onNext(List<SMSBean> list) {
//                       Log.e(TAG,"onNext");
//                       smsBeanList=list;
//                       if (smsBeanList==null||smsBeanList.size()==0){
//                           Log.e(TAG,"SHOW");
//                           NodataImg.setVisibility(View.VISIBLE);
//                           recyclerView.setVisibility(View.GONE);
//                       }else {
//                           Log.e(TAG,"NOT SHOW");
//                           NodataImg.setVisibility(View.GONE);
//                           recyclerView.setVisibility(View.VISIBLE);
//                           messageAdapter.notifyDataSetChanged();
//                       }
//                   }
    }

    /**
     * 刷新数据
     */
    private void refeshdata(List<SMSBean> list){
        smsBeanList=list;
        if (smsBeanList==null||smsBeanList.size()==0){
            Log.e(TAG,"SHOW");
            NodataImg.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else {
            Log.e(TAG,"NOT SHOW"+list.size());
            NodataImg.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            messageAdapter.NotifyData(list);
        }
    }
}