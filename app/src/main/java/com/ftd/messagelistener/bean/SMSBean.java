package com.ftd.messagelistener.bean;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.ftd.messagelistener.utils.StringUtils;

import java.io.Serializable;

/**
 * tangxianfeng
 * 2021.11.28
 */
@Entity
public class SMSBean implements Serializable {

    @PrimaryKey(autoGenerate = true)
    public int sid;

    @ColumnInfo(name = "message")
    private String message;

    @ColumnInfo(name = "phone")
    private  String phone;

    @ColumnInfo(name = "person")
    private  String person;

    @ColumnInfo(name = "date")
    private  String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getMessage() {
        if (StringUtils.isEmpty(message)){
            return "该短信内容为空";
        }
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPerson() {
        return person;
    }

    public void setPerson(String person) {
        this.person = person;
    }

}
