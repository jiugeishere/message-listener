package com.ftd.messagelistener.bean;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

/**
 * tangxianfeng
 * 2021.11.28
 */
@Entity
public class InterceptorBean {
    @PrimaryKey(autoGenerate = true)
    public int Iid;

    @ColumnInfo(name = "configname")
    private String configname;

    @ColumnInfo(name = "interceptorcontent")
    private String interceptorcontent;

    @ColumnInfo(name = "interceptorsend")
    private String interceptorsend;

    public String getConfigname() {
        return configname;
    }

    public void setConfigname(String configname) {
        this.configname = configname;
    }

    public String getInterceptorcontent() {
        return interceptorcontent;
    }

    public void setInterceptorcontent(String interceptorcontent) {
        this.interceptorcontent = interceptorcontent;
    }

    public String getInterceptorsend() {
        return interceptorsend;
    }

    public void setInterceptorsend(String interceptorsend) {
        this.interceptorsend = interceptorsend;
    }

}
