package com.ftd.messagelistener.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.utils.SharePrefenceUtil;
import com.ftd.messagelistener.utils.StatusBarUtils;
import com.ftd.messagelistener.utils.view.ChoseCommonDialog;

/**
 * 闹钟配置页
 */
public class AlarmConfigActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_alarm_config);
        StatusBarUtils.darkMode(this, true);//沉浸式状态栏
        initView();
    }

    private void initView(){
        ImageView back=findViewById(R.id.back);
        back.setOnClickListener(view -> finish());
        RelativeLayout nz_congfig_rela_test=findViewById(R.id.nz_congfig_rela_test);
        nz_congfig_rela_test.setOnClickListener(view ->  startActivity(new Intent(this, AlarmActivity.class)));
        RelativeLayout nz_congfig_rela_music=findViewById(R.id.nz_congfig_rela_music);
        RelativeLayout nz_congfig_repeat_music=findViewById(R.id.nz_congfig_rela_repeat);
        TextView repeat=findViewById(R.id.nz_config_repeat_choose);
        TextView music=findViewById(R.id.nz_config_music_choose);
        if (SharePrefenceUtil.getValue(AlarmConfigActivity.this,"MUSIC_CONFIG_NAOZHONG",false)){
            music.setText("使用系统闹钟铃声");
        }
        if (SharePrefenceUtil.getValue(AlarmConfigActivity.this,"NOREPEAT_CONFIG_NAOZHONG",false)){
            repeat.setText("仅提醒一声");
        }


        nz_congfig_rela_music.setOnClickListener(view -> {
            ChoseCommonDialog choseCommonDialog=new ChoseCommonDialog(AlarmConfigActivity.this);
            choseCommonDialog.setListener(new ChoseCommonDialog.DialogClick() {
                @Override
                public void choosContent1() {
                    music.setText("使用系统电话铃声");
                    choseCommonDialog.dismiss();
                    SharePrefenceUtil.putValue(AlarmConfigActivity.this,"MUSIC_CONFIG_NAOZHONG",false);
                }

                @Override
                public void choosContent2() {
                    music.setText("使用系统闹钟铃声");
                    choseCommonDialog.dismiss();
                    SharePrefenceUtil.putValue(AlarmConfigActivity.this,"MUSIC_CONFIG_NAOZHONG",true);

                }
            },"使用系统电话铃声","使用系统闹钟铃声");
            choseCommonDialog.show();
        });
        nz_congfig_repeat_music.setOnClickListener(view -> {
            ChoseCommonDialog choseCommonDialog=new ChoseCommonDialog(AlarmConfigActivity.this);
            choseCommonDialog.setListener(new ChoseCommonDialog.DialogClick() {
                @Override
                public void choosContent1() {
                    repeat.setText("一直循环重复");
                    choseCommonDialog.dismiss();
                    SharePrefenceUtil.putValue(AlarmConfigActivity.this,"NOREPEAT_CONFIG_NAOZHONG",false);
                }

                @Override
                public void choosContent2() {
                    repeat.setText("仅提醒一声");
                    choseCommonDialog.dismiss();
                    SharePrefenceUtil.putValue(AlarmConfigActivity.this,"NOREPEAT_CONFIG_NAOZHONG",true);

                }
            },"一直循环重复","仅提醒一声");
            choseCommonDialog.show();
        });

    }

}