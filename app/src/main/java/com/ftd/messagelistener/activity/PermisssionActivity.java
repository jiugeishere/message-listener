package com.ftd.messagelistener.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.base.Constants;
import com.ftd.messagelistener.service.SMSService;
import com.ftd.messagelistener.utils.AliveUtil;
import com.ftd.messagelistener.utils.DefaultPermissionInterceptor;
import com.ftd.messagelistener.utils.StatusBarUtils;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;

public class PermisssionActivity extends AppCompatActivity {

    private ImageView SMSImg;
    private ImageView Windows;
    private ImageView Stroge;
    private ImageView Nofication;
    private ImageView Battery;
    private TextView batteryTv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_permisssion);
        StatusBarUtils.darkMode(this, true);//沉浸式状态栏
        initView();
        initpermission();
    }

    private void initView() {
        SMSImg = findViewById(R.id.permission_sms);
        Stroge = findViewById(R.id.permission_stroge);
        Windows = findViewById(R.id.permission_windowns);
        Nofication = findViewById(R.id.permission_notification);
        Battery = findViewById(R.id.permission_battery);
        batteryTv=findViewById(R.id.battery_tv);
        ImageView back=findViewById(R.id.back);
        back.setOnClickListener(view -> finish());
        Button service_bt=findViewById(R.id.service_bt);
        service_bt.setOnClickListener(view -> XXPermissions.with(PermisssionActivity.this)
                .permission(Permission.READ_SMS,
                        Permission.RECEIVE_SMS,
                        Permission.WRITE_EXTERNAL_STORAGE,
                        Permission.READ_EXTERNAL_STORAGE,
                        Permission.NOTIFICATION_SERVICE,
                        Permission.SYSTEM_ALERT_WINDOW)
                .request((permissions, all) -> {
                    if (all){
                        Intent ser=new Intent(PermisssionActivity.this, SMSService.class);
                        ser.setAction(Constants.SMSUpdataAction);//发出更新指令
                        startService(ser);
                    }else {
                        Toast.makeText(PermisssionActivity.this,"权限未通过后台服务暂未开启",Toast.LENGTH_LONG).show();
                    }
                }));
    }

    private void initpermission() {
        if (!XXPermissions.isGranted(this, Permission.RECEIVE_SMS, Permission.READ_SMS)) {
            SMSImg.setImageResource(R.drawable.ic_wrong);
            SMSImg.setOnClickListener(view -> getPermission(new String[]{Permission.RECEIVE_SMS, Permission.READ_SMS},SMSImg));
        }
        if (!XXPermissions.isGranted(this, Permission.WRITE_EXTERNAL_STORAGE,
                Permission.READ_EXTERNAL_STORAGE)) {
            Stroge.setImageResource(R.drawable.ic_wrong);

            Stroge.setOnClickListener(view -> getPermission(new String[]{Permission.WRITE_EXTERNAL_STORAGE,
                    Permission.READ_EXTERNAL_STORAGE},Stroge));
        }
        if (!XXPermissions.isGranted(this, Permission.NOTIFICATION_SERVICE)) {
            Nofication.setImageResource(R.drawable.ic_wrong);

            Nofication.setOnClickListener(view -> getPermission(new String[]{Permission.NOTIFICATION_SERVICE},Nofication));
        }
        if (!XXPermissions.isGranted(this, Permission.SYSTEM_ALERT_WINDOW)) {
            Windows.setImageResource(R.drawable.ic_wrong);
            Windows.setOnClickListener(view -> getPermission(new String[]{Permission.SYSTEM_ALERT_WINDOW},Windows));

        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!AliveUtil.isIgnoringBatteryOptimizations(PermisssionActivity.this)){
                Battery.setImageResource(R.drawable.ic_wrong);
                Battery.setOnClickListener(view -> AliveUtil.requestIgnoreBatteryOptimizations(PermisssionActivity.this));
            }
        }else {
            batteryTv.setText("本手机版本较低，暂无法检测电池优化方案");
        }
    }

    private void getPermission(String[] permission,ImageView imageView){
        XXPermissions.with(PermisssionActivity.this)
                .interceptor(new DefaultPermissionInterceptor())
                .permission(permission)
                .request((permissions, all) -> {
                    if (all){
                        imageView.setImageResource(R.drawable.ic_safe);
                    }
                });
    }
}