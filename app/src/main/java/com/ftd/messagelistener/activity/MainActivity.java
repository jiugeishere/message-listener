package com.ftd.messagelistener.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.viewpager.widget.ViewPager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.widget.Toast;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.adapter.CustomFragmentAdapter;
import com.ftd.messagelistener.fragment.ConfigFragment;
import com.ftd.messagelistener.fragment.MessageListFragment;
import com.ftd.messagelistener.service.SMSService;
import com.ftd.messagelistener.utils.AliveUtil;
import com.ftd.messagelistener.utils.StatusBarUtils;
import com.google.android.material.tabs.TabLayout;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * tangxianfeng
 * 2021.11.28
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG="MainActivity";

    private final List<Fragment> fragmentList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.e(TAG,"onCreate");
        fragmentList.add(new MessageListFragment());//左边第一个是
        fragmentList.add(new ConfigFragment());//第二个

        XXPermissions.with(this)
                .permission(Permission.READ_SMS,
                        Permission.RECEIVE_SMS,
                        Permission.WRITE_EXTERNAL_STORAGE,
                        Permission.READ_EXTERNAL_STORAGE,
                        Permission.NOTIFICATION_SERVICE,
                        Permission.SYSTEM_ALERT_WINDOW)
                .request((permissions, all) -> {
                    if (all){
                        Log.e(TAG,"GET ALL Permission");
                        registService();
                    }else {
                        Toast.makeText(MainActivity.this,"权限未通过后台服务暂未开启",Toast.LENGTH_LONG).show();
                    }
                });
        initView();
    }


    private void initView(){
        StatusBarUtils.darkMode(this, true);//沉浸式状态栏
        TabLayout tab_layout = findViewById(R.id.main_tab);
        ViewPager viewPager = findViewById(R.id.main_viewpage);
        CustomFragmentAdapter fragmentAdater = new  CustomFragmentAdapter(getSupportFragmentManager(), FragmentPagerAdapter.BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT,fragmentList, Arrays.asList("拦截列表", "配置页"));
        viewPager.setAdapter(fragmentAdater);
        tab_layout.setupWithViewPager(viewPager);
    }

    @Override
    public void finish() {
        Log.e(TAG,"moveTaskToBack");
        moveTaskToBack(true);
        //super.finish();
    }

    @Override
    protected void onDestroy() {
        Log.e(TAG,"onDestroy");
       // unbindService(conn);
        unregistService();
        super.onDestroy();
    }



    private void registService(){
        Intent Messageservice=new Intent(getApplicationContext(),SMSService.class);
        Messageservice.setAction(SMSService.ACTION);
        startService(Messageservice);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {//android8.0以上通过startForegroundService启动service
           startForegroundService(Messageservice);
        } else {
           startService(Messageservice);
        }
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                AliveUtil.checkBatteryStrategy(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //bindService(Messageservice,conn,Context.BIND_ABOVE_CLIENT);
    }

    private void unregistService(){
        Intent Messageservice=new Intent(getApplicationContext(),SMSService.class);
        Messageservice.setAction(SMSService.ACTION);
        stopService(Messageservice);
    }

    @Override
    protected void onResume() {

        super.onResume();

    }
}