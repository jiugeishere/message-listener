package com.ftd.messagelistener.activity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Build;
import android.os.Bundle;
import android.view.WindowManager;

import com.ftd.messagelistener.bean.SMSBean;
import com.ftd.messagelistener.utils.DateUtil;
import com.ftd.messagelistener.utils.LockerHelper;
import com.ftd.messagelistener.utils.StatusBarUtils;

public class AlarmActivity extends AppCompatActivity {

//    private LEDView ledView;
//    private Ringtone music;
//    private final Handler mHandler = new Handler();
//    private final Runnable mColockReplay = new Runnable() {
//
//        @Override
//        public void run() {
//            if (music==null){
//                playAlarmMediaPlayer();
//            }else if (!music.isPlaying()){
//                music.play();
//            }
//            mHandler.postDelayed(this, 10000);
//        }
//    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        //设置当前Activity的锁屏显示
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED //锁屏显示
                | WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD //解锁
                | WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON //保持屏幕不息屏
                | WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);//点亮屏幕
        // 息屏页显示
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O_MR1) {
            setShowWhenLocked(true);
        }
        StatusBarUtils.darkMode(this, true);//沉浸式状态栏
        SMSBean smsBean= (SMSBean) getIntent().getSerializableExtra("SMSBean");
        if (smsBean==null){
            smsBean=new SMSBean();
            smsBean.setMessage("这是一条测试内容");
            smsBean.setDate(DateUtil.getStringDate());
            smsBean.setPhone("10086");
        }
        LockerHelper.getInstance(this).show(smsBean,this::finish);
        super.onCreate(savedInstanceState);
    }

    @Override
    protected void onResume() {
        //setContentView(R.layout.activity_alarm);
        super.onResume();
    }

    @Override
    public void onBackPressed() {
       super.onBackPressed();
    }



    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
}