package com.ftd.messagelistener.activity;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.Toast;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.adapter.InterceptorAdapter;
import com.ftd.messagelistener.base.Constants;
import com.ftd.messagelistener.bean.InterceptorBean;
import com.ftd.messagelistener.data.AppDatabase;
import com.ftd.messagelistener.service.SMSService;
import com.ftd.messagelistener.utils.StatusBarUtils;
import com.ftd.messagelistener.utils.view.EditInterceptorDialog;
import com.hjq.permissions.Permission;
import com.hjq.permissions.XXPermissions;

import java.util.ArrayList;
import java.util.List;

public class InterceptorActivity extends AppCompatActivity {
    private static final String TAG="InterceptorActivity";

    private InterceptorAdapter interceptorAdapter;

    private List<InterceptorBean> interceptorBeans=new ArrayList<>();
    private RecyclerView recyclerView;
    private AppCompatImageView NodataImg;//无数据展示图
    private ImageView updata;//更新图像
    private RotateAnimation animation;

    private EditInterceptorDialog editInterceptorDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_interceptor);
        StatusBarUtils.darkMode(this, true);//沉浸式状态栏
        initView();
    }

    private void initView(){
        recyclerView=findViewById(R.id.interceptor_recycler);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        ImageView back=findViewById(R.id.back);
        back.setOnClickListener(view -> finish());
        CardView cardView=findViewById(R.id.interceptor_add);
        cardView.setOnClickListener(view -> {
            InterceptorBean interceptorBean=new InterceptorBean();
            interceptorBean.setConfigname("配置"+(interceptorBeans.size()+1));
            if (editInterceptorDialog==null){
                editInterceptorDialog=new EditInterceptorDialog(InterceptorActivity.this,interceptorBean);
            }else {
                editInterceptorDialog.updateInteceptor(interceptorBean);
            }
            if (!editInterceptorDialog.isShowing()){
                editInterceptorDialog.show();
            }
        });

        NodataImg=findViewById(R.id.nodata_img);
        if (interceptorAdapter==null){
            interceptorAdapter=new InterceptorAdapter(this,interceptorBeans);
        }
        recyclerView.setAdapter(interceptorAdapter);

        updata=findViewById(R.id.updata);
        RotateAnimation();
        updata.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                updata.startAnimation(animation);
                updateService();
            }
        });
        AppDatabase.getInstance(this)
                .interceptordao()
                .getAll()
                .observe(this, this::refeshdata);
    }

    private void RotateAnimation(){
        if (animation==null){
            animation =new RotateAnimation(0f,360f,Animation.RELATIVE_TO_SELF,
                    0.5f, Animation.RELATIVE_TO_SELF,0.5f);
            animation.setDuration(1000);//设置动画持续时间
        }
    }

    /**
     * 刷新数据
     */
    private void refeshdata(List<InterceptorBean> list){
        interceptorBeans=list;
        if (interceptorBeans==null||interceptorBeans.size()==0){
            Log.e(TAG,"SHOW");
            NodataImg.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }else {
            Log.e(TAG,"NOT SHOW"+list.get(0).toString());
            NodataImg.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            interceptorAdapter.NotifyData(list);
            updateService();
        }
    }

    private void updateService(){
        XXPermissions.with(InterceptorActivity.this)
                .permission(Permission.READ_SMS,
                        Permission.RECEIVE_SMS,
                        Permission.WRITE_EXTERNAL_STORAGE,
                        Permission.READ_EXTERNAL_STORAGE,
                        Permission.NOTIFICATION_SERVICE,
                        Permission.SYSTEM_ALERT_WINDOW)
                .request((permissions, all) -> {
                    if (all){
                        Intent ser=new Intent(InterceptorActivity.this, SMSService.class);
                        ser.setAction(Constants.SMSUpdataAction);//发出更新指令
                        startService(ser);
                    }else {
                        Toast.makeText(InterceptorActivity.this,"权限未通过后台服务暂未开启",Toast.LENGTH_LONG).show();
                    }
                });
    }
}