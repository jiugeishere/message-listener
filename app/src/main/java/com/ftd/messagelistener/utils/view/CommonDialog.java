package com.ftd.messagelistener.utils.view;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import com.ftd.messagelistener.R;

public class CommonDialog extends AlertDialog {
    private TextView tvContent;
    private TextView tvCancel;
    private TextView tvSure;

    private String content;
    private View.OnClickListener cancelListener;
    private View.OnClickListener sureListener;

    private Context context;

    public CommonDialog(@NonNull Context context) {
        super(context, R.style.dialog_style);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setCancelable(false);
        View view = LayoutInflater.from(context).inflate(R.layout.dialog_common, null);
        setContentView(view);
        tvContent = view.findViewById(R.id.tv_content);
        tvContent.setText(content);
        tvCancel = view.findViewById(R.id.tv_cancel);
        if (null != cancelListener) {
            tvCancel.setOnClickListener(cancelListener);
        }
        tvSure = view.findViewById(R.id.tv_sure);
        if (null != sureListener) {
            tvSure.setOnClickListener(sureListener);
        }
    }

    public CommonDialog setContent(String content) {
        this.content = content;
        return this;
    }

    public CommonDialog setSureListener(View.OnClickListener listener) {
        this.sureListener = listener;
        return this;
    }

    public CommonDialog setCancelClickListener(View.OnClickListener listener) {
        this.cancelListener = listener;
        return this;
    }
}
