package com.ftd.messagelistener.utils.view;

import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.util.Log;

import com.ftd.messagelistener.R;


public class CircleWaveView extends android.view.View {

    //画笔(第一个波纹)
    private Paint mpaint;
    //透明度
    private Integer alpha = 255;
    //扩散颜色
    private int mColor;
    //圆半径
    private float mCoreRadius = 0;
    //扩散动画控制
    private ValueAnimator valueAnimator;
    //画笔(第二个波纹)
    private Paint mpaint2;
    //透明度(第二个波纹)
    private Integer alpha2 = 255;
    //圆半径(第二个波纹)
    private float mCoreRadius2 = 0;
    //扩散动画控制(第二个波纹)
    private ValueAnimator valueAnimator2;
    //画笔(第三个波纹)
    private Paint mpaint3;
    //透明度(第三个波纹)
    private Integer alpha3 = 255;
    //圆半径(第三个波纹)
    private float mCoreRadius3 = 0;
    //扩散动画控制(第三个波纹)
    private ValueAnimator valueAnimator3;
    //控制变量
    private boolean control = false;
    private int maxRaduis = 40;
    private int minRaduis = 20;


    public CircleWaveView(Context context, @androidx.annotation.Nullable AttributeSet attrs) {
        super(context, attrs);
        //也可通过attr进行xml文件内的自定义设置
        if (attrs != null) {
            @SuppressLint({"Recycle", "CustomViewStyleable"}) TypedArray typedArray = context.obtainStyledAttributes(attrs, R.styleable.MyCircleWave);
            if (typedArray.getString(R.styleable.MyCircleWave_MyCircleWaveColor) == null) {
                //获取自定义属性（默认设置）
                mColor = Color.WHITE;
            } else {
                mColor = Color.parseColor(typedArray.getString(R.styleable.MyCircleWave_MyCircleWaveColor));
            }
            Log.e("BRaduis", maxRaduis + ":" + minRaduis);
            maxRaduis = (int) typedArray.getDimension(R.styleable.MyCircleWave_MyCircleWaveMaxRadius, 40);
            minRaduis = (int) typedArray.getDimension(R.styleable.MyCircleWave_MyCircleWaveMinRadius, 20);
        }
        init();
    }


    private void init() {
        mpaint = new Paint();
        mpaint2 = new Paint();
        mpaint3 = new Paint();
        mpaint.setColor(mColor);
        mpaint2.setColor(mColor);
        mpaint3.setColor(mColor);

        valueAnimator3 = ValueAnimator.ofInt(minRaduis, maxRaduis);
        valueAnimator3.setDuration(3000);
        valueAnimator3.addUpdateListener(animation -> {
            alpha3 = 255- maxRaduis + (int) animation.getAnimatedValue();
            mCoreRadius3 = (int) animation.getAnimatedValue();
        });
        valueAnimator3.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator3.setRepeatMode(ValueAnimator.RESTART);

        valueAnimator2 = ValueAnimator.ofInt(minRaduis, maxRaduis);
        valueAnimator2.setDuration(3000);
        valueAnimator2.addUpdateListener(animation -> {
            alpha2 = 255- maxRaduis + (int) animation.getAnimatedValue();
            mCoreRadius2 = (int) animation.getAnimatedValue();
        });
        valueAnimator2.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator2.setRepeatMode(ValueAnimator.RESTART);

        valueAnimator = ValueAnimator.ofInt(minRaduis, maxRaduis);
        valueAnimator.setDuration(3000);
        valueAnimator.addUpdateListener(animation -> {
            alpha =255- maxRaduis + (int) animation.getAnimatedValue();
            mCoreRadius = (int) animation.getAnimatedValue();
            invalidate();
        });
        valueAnimator.setRepeatCount(ValueAnimator.INFINITE);
        valueAnimator.setRepeatMode(ValueAnimator.RESTART);

    }


    /**
     * 进行圆形重绘
     *
     * @param canvas
     */
    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        // 绘制扩散圆
        mpaint.setAlpha(alpha);
        mpaint2.setAlpha(alpha2);
        mpaint3.setAlpha(alpha3);
        int c=getWidth() / 2;
        canvas.drawCircle(c, c, mCoreRadius, mpaint);
        canvas.drawCircle(c, c, mCoreRadius2, mpaint2);
        canvas.drawCircle(c, c, mCoreRadius3, mpaint3);
    }

    public void setRadius(int maxRaduis, int minRaduis) {
        this.maxRaduis = maxRaduis;
        this.minRaduis = minRaduis;

    }

    /**
     * 重新开始扩散
     */
    public void restart() {
        if (!control) {
            valueAnimator.start();
            valueAnimator2.setStartDelay(1000);
            valueAnimator2.start();
            valueAnimator3.setStartDelay(2500);
            valueAnimator3.start();
            control = true;
        }
        invalidate();
    }

    public boolean getState() {
        return control;
    }

    /**
     * 暂停扩散
     */
    public void start() {
        valueAnimator.start();
        valueAnimator2.start();
        valueAnimator3.start();
        control = true;
    }
    /**
     * 暂停扩散
     */
    public void pause() {
        valueAnimator.pause();
        valueAnimator2.pause();
        valueAnimator3.pause();
        control = false;
    }

    /**
     * 停止扩散
     */
    public void stop() {
        if (control) {
            valueAnimator.pause();
            valueAnimator2.pause();
            valueAnimator3.pause();
        }
        control = false;
        alpha = 0;
        alpha2 = 0;
        alpha3 = 0;
        mCoreRadius = 0;
        mCoreRadius2 = 0;
        mCoreRadius3 = 0;
        invalidate();
    }
}


