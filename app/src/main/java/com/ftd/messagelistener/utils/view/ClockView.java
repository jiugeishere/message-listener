package com.ftd.messagelistener.utils.view;

import java.io.File;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Typeface;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.base.Constants;
import com.ftd.messagelistener.utils.DateUtil;

public class ClockView extends LinearLayout {

    private TextView timeView;
    private TextView timeView2;

    private static final int REFRESH_DELAY = 500;

    private final Handler mHandler = new Handler();
    private final Runnable mTimeRefresher = new Runnable() {

        @Override
        public void run() {
            Calendar calendar = Calendar.getInstance(TimeZone
                    .getTimeZone("GMT+8"));
            final Date d = new Date();
            calendar.setTime(d);
            String ymddate= DateUtil.getCurrentTimeYMD();
            String hmsdate= DateUtil.getCurrentTimeHMS();
            timeView.setText(ymddate);
            timeView2.setText(hmsdate);
            mHandler.postDelayed(this, REFRESH_DELAY);
        }
    };

    @SuppressLint("NewApi")
    public ClockView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context);
    }

    public ClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public ClockView(Context context) {
        super(context);
        init(context);
    }

    private void init(Context context) {
        LayoutInflater layoutInflater = LayoutInflater.from(context);

        View view = layoutInflater.inflate(R.layout.ledview, this);
        timeView = (TextView) view.findViewById(R.id.ledview_clock_time);
        timeView2 = (TextView) view.findViewById(R.id.ledview_clock_time2);
        AssetManager assets = context.getAssets();
        Typeface font = Typeface.createFromAsset(assets, Constants.FONT_COLOCK);
        timeView.setTypeface(font);// 设置字体
        timeView2.setTypeface(font);// 设置字体

    }

    public void start() {
        mHandler.post(mTimeRefresher);
    }

    public void stop() {
        mHandler.removeCallbacks(mTimeRefresher);
    }
}
