package com.ftd.messagelistener.utils.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.bean.InterceptorBean;
import com.ftd.messagelistener.data.AppDatabase;
import com.ftd.messagelistener.utils.DateUtil;
import com.ftd.messagelistener.utils.StringUtils;
import com.google.android.material.textfield.TextInputEditText;

import java.util.concurrent.Executors;

public class EditInterceptorDialog extends Dialog {
    private InterceptorBean interceptorBean;
    private final Context mContext;


    private TextView SaveTv;
    private TextView DismissTv;
    private TextInputEditText KeywordET;
    private TextInputEditText SendET;
    private TextInputEditText ConfignameEt;
    private int mode=0; //0 添加 1更新 2查看

    public EditInterceptorDialog(@NonNull Context context, InterceptorBean interceptorBean) {
        super(context);
        this.mContext = context;
        this.interceptorBean = interceptorBean;
        mode=0;
    }

    public EditInterceptorDialog(@NonNull Context context, InterceptorBean interceptorBean,int mode) {
        super(context);
        this.mContext = context;
        this.interceptorBean = interceptorBean;
        this.mode=mode;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_editinterceptor);
        setCanceledOnTouchOutside(true);
        initView();
        initConfig();
        initListener();
    }

    private void initView() {
        SaveTv = findViewById(R.id.dialoginter_save_tv);
        DismissTv = findViewById(R.id.dialoginter_dismiss_tv);
        KeywordET = findViewById(R.id.dialoginter_keyword_et);
        SendET = findViewById(R.id.dialoginter_sendname_et);
        ConfignameEt = findViewById(R.id.dialoginter_configname_et);
    }

    private void initListener(){

        DismissTv.setOnClickListener(view -> dismiss());

        //保存到数据库
        SaveTv.setOnClickListener(view -> {
            interceptorBean.setConfigname(ConfignameEt.getText().toString());
            interceptorBean.setInterceptorsend(SendET.getText().toString());
            interceptorBean.setInterceptorcontent(KeywordET.getText().toString());
            if (StringUtils.isEmpty(interceptorBean.getInterceptorcontent()) && StringUtils.isEmpty(interceptorBean.getInterceptorsend())) {
                Toast.makeText(mContext, "发送号码和拦截词不能都为空", Toast.LENGTH_LONG).show();
                return;
            } else if (StringUtils.isEmpty(interceptorBean.getConfigname())) {
                interceptorBean.setConfigname(DateUtil.getCurrentTimeYMD() + "的配置");
            }
            if (mode==0){
                Executors.newSingleThreadExecutor().execute(() -> AppDatabase.getInstance(mContext).interceptordao().insert(interceptorBean));
            }else if (mode==1){
                Executors.newSingleThreadExecutor().execute(() -> AppDatabase.getInstance(mContext).interceptordao().update(interceptorBean));
            }
            dismiss();
        });
    }
    //初始化配置
    private void initConfig(){
        if (!StringUtils.isEmpty(interceptorBean.getConfigname())){
            ConfignameEt.setText(interceptorBean.getConfigname());
        }
        if (!StringUtils.isEmpty(interceptorBean.getInterceptorsend())){
            SendET.setText(interceptorBean.getInterceptorsend());
        }
        if (!StringUtils.isEmpty(interceptorBean.getInterceptorcontent())){
            KeywordET.setText(interceptorBean.getInterceptorcontent());
        }

        if (mode==0){
            SaveTv.setText("保存");
        }else if (mode==1){
            SaveTv.setText("更新");
        }

        //查看模式
        SaveTv.setVisibility(mode==2?View.GONE:View.VISIBLE);
        //是否可编辑
        SendET.setEnabled(mode != 2);
        ConfignameEt.setEnabled(mode != 2);
        KeywordET.setEnabled(mode != 2);
    }

    public void updateInteceptor(InterceptorBean interceptorBean){
        this.interceptorBean=interceptorBean;
        initConfig();
    }
}
