package com.ftd.messagelistener.utils;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;

import com.ftd.messagelistener.bean.SMSBean;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
/**
 * tangxianfeng
 * 2021.11.28
 */
public class SMSUtils {


    /**
     * 获取手机所有通知类信息
     * @param context
     * @return
     */
    public static List<SMSBean> getSMS(Context context) {
        List<SMSBean> smsBeanList=new ArrayList<>();
        final String SMS_URI_INBOX = "content://sms/inbox";

        Uri uri = Uri.parse(SMS_URI_INBOX);
        String[] projection = new String[]{"_id", "address", "person", "body", "date", "type",};
        Cursor cur = context.getContentResolver().query(uri, projection, "read = ?", new String[]{"0"}, "date desc");
        if (null == cur) {
            Log.i("ooc","************cur == null");
            return smsBeanList;
        }

        if(cur.moveToFirst()){
            int index_Address = cur.getColumnIndex("address");
            int index_Person = cur.getColumnIndex("person");
            int index_Body = cur.getColumnIndex("body");
            int index_Date = cur.getColumnIndex("date");
            int index_Type = cur.getColumnIndex("type");

            do {
                String strAddress = cur.getString(index_Address);
                int strPerson = cur.getInt(index_Person);
                String strbody = cur.getString(index_Body);
                long longDate = cur.getLong(index_Date);
                int intType = cur.getInt(index_Type);

                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss",Locale.CHINESE);
                Date d = new Date(longDate);
                String strDate = dateFormat.format(d);

                Log.e("SMSUtils",strAddress+"\n:"+strPerson+"\n:"+strbody+":\n");

 //               String strType = "";
                if (intType == 1) {
                    SMSBean smsBean=new SMSBean();
                    smsBean.setDate(strDate);
                    smsBean.setMessage(strbody);
                    smsBean.setPhone(strAddress);
                    if (strPerson!=0) {
                        smsBean.setPerson(strPerson + "");
                    }
                    smsBeanList.add(smsBean);
                }
//                if (intType == 2) {
//                    strType = "发送";
//                } else if (intType == 3) {
//                    strType = "草稿";
//                } else if (intType == 4) {
//                    strType = "发件箱";
//                } else if (intType == 5) {
//                    strType = "发送失败";
//                } else if (intType == 6) {
//                    strType = "待发送列表";
//                } else if (intType == 0) {
//                    strType = "所以短信";
//                } else {
//                    strType = "null";
//                }

            } while (cur.moveToNext());

            if (!cur.isClosed()) {
                cur.close();
            }
        }


        return smsBeanList;
    }
}
