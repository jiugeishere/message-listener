package com.ftd.messagelistener.utils;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Build;
import android.os.PowerManager;
import android.provider.Settings;
import android.util.Log;
import android.view.View;

import androidx.annotation.RequiresApi;

import com.ftd.messagelistener.utils.view.CommonDialog;

/**
 * Created by liangenshuo on 2021/10/11
 */
public class AliveUtil {
    private static String TAG = "AliveUtil";

    @RequiresApi(api = Build.VERSION_CODES.M)
    public static void checkBatteryStrategy(Context context) {
        if (!isIgnoringBatteryOptimizations(context)) {
            showIgnoreBatteryDialog(context);
        }
    }

    @RequiresApi(Build.VERSION_CODES.M)
    public static boolean isIgnoringBatteryOptimizations(Context context) {
        boolean isIgnoring = false;
        PowerManager powerManager = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
        if (powerManager != null) {
            isIgnoring = powerManager.isIgnoringBatteryOptimizations(context.getPackageName());
        }
        return isIgnoring;
    }

    public static void showIgnoreBatteryDialog(Context context) {
        Log.e(TAG, "onNext: ");
        if (null != context) {
            CommonDialog dialog = new CommonDialog(context);
            dialog.setContent("为保证应用在后台时能正常运行，需要更改系统的电池优化方案");
            dialog.setSureListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.e(TAG, "onClick: ");
                    requestIgnoreBatteryOptimizations(context);
                    dialog.dismiss();
                }
            });
            dialog.setCancelClickListener(view -> dialog.dismiss());
            dialog.show();
        }
    }

    public static void requestIgnoreBatteryOptimizations(Context context) {
        try {
            Intent intent = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                Log.e(TAG, "requestIgnoreBatteryOptimizations1: ");
                intent = new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
            }else {
                Log.e(TAG, "requestIgnoreBatteryOptimizations2: " );
                Intent powerUsageIntent = new Intent(Intent.ACTION_POWER_USAGE_SUMMARY);
                ResolveInfo resolveInfo = context.getPackageManager().resolveActivity(powerUsageIntent, 0);
                if(resolveInfo != null){
                    Log.e(TAG, "requestIgnoreBatteryOptimizations3: " );
                    context.startActivity(powerUsageIntent);
                }
            }
            if (intent != null) {
                Log.e(TAG, "requestIgnoreBatteryOptimizations4: " );
                intent.setData(Uri.parse("package:" + context.getPackageName()));
            }
            Log.e(TAG, "requestIgnoreBatteryOptimizations5: " );
            context.startActivity(intent);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "requestIgnoreBatteryOptimizations: " + e.getLocalizedMessage());
        }
    }
}
