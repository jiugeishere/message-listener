package com.ftd.messagelistener.utils.view;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.utils.DisplayUtil;


public class ChoseCommonDialog extends Dialog implements View.OnClickListener {

    private DialogClick listener;
    private String Scontent1;
    private String Scontent2;


    public ChoseCommonDialog(@NonNull Context context) {
        this(context, R.style.dialog_style);
    }

    public ChoseCommonDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    protected ChoseCommonDialog(@NonNull Context context, boolean cancelable, @Nullable OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_chose_formbottom);
        TextView content1 = findViewById(R.id.content1);
        TextView content2 = findViewById(R.id.content2);
        TextView cancel = findViewById(R.id.tvCancel);
        content1.setOnClickListener(this);
        content2.setOnClickListener(this);
        cancel.setOnClickListener(this);

        content1.setText(Scontent1);
        content2.setText(Scontent2);
        setCancelable(false);
        setCanceledOnTouchOutside(true);
        Window window = getWindow();
        window.setGravity(Gravity.BOTTOM);
        window.setWindowAnimations(R.style.BottomDialogStyle);
        WindowManager.LayoutParams lp = window.getAttributes();
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = DisplayUtil.dip2px(getContext(), 166);
        getWindow().setAttributes(lp);
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.content1) {
            listener.choosContent1();
        } else if (v.getId() == R.id.content2) {
            listener.choosContent2();
        } else if (v.getId() == R.id.tvCancel) {
            dismiss();
        }
    }


    public void setListener(DialogClick listener, String Scontent1, String Scontent2) {
        this.listener = listener;
        this.Scontent1 = Scontent1;
        this.Scontent2 = Scontent2;
    }

    /**
     * 监听
     */
    public interface DialogClick {
        void choosContent1();

        void choosContent2();

    }
}
