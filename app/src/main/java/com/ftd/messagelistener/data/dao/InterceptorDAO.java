package com.ftd.messagelistener.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.ftd.messagelistener.bean.InterceptorBean;
import com.ftd.messagelistener.bean.SMSBean;

import java.util.List;

import io.reactivex.Observable;

@Dao
public interface InterceptorDAO {

    @Query("SELECT * FROM interceptorbean")
    Observable<List<InterceptorBean>> getAllByObservable();

    @Query("SELECT * FROM interceptorbean")
    LiveData<List<InterceptorBean>> getAll();

    @Update
    void update(InterceptorBean interceptorBean);

    @Insert
    void insert(InterceptorBean interceptorBean);

    @Delete
    void delete(InterceptorBean interceptorBean);
}
