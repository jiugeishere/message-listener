package com.ftd.messagelistener.data.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import com.ftd.messagelistener.bean.SMSBean;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.Observable;
import io.reactivex.Observer;

@Dao
public interface SMSDAO {

    @Query("SELECT * FROM smsbean")
    LiveData<List<SMSBean>> getAll();

    @Query("SELECT * FROM smsbean WHERE phone LIKE :p ")
    List<SMSBean> findByPhone(String p);


    @Query("SELECT * FROM smsbean WHERE phone LIKE  '%'||:source||'%' OR message LIKE '%'||:source||'%' OR person LIKE '%'||:source||'%' OR date LIKE '%'||:source||'%'")
    Observable<List<SMSBean>> findBySource(String source);//关键字查询

    @Insert
    void insertAll(SMSBean... smsBeans);

    @Insert
    void insert(SMSBean smsBean);

    @Delete
    void delete(SMSBean smsBean);
}
