package com.ftd.messagelistener.data;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.ftd.messagelistener.base.Constants;
import com.ftd.messagelistener.bean.InterceptorBean;
import com.ftd.messagelistener.bean.SMSBean;
import com.ftd.messagelistener.data.dao.InterceptorDAO;
import com.ftd.messagelistener.data.dao.SMSDAO;
@Database(entities = {SMSBean.class, InterceptorBean.class} ,version = 1,exportSchema = false)
public abstract class AppDatabase extends RoomDatabase {

    public abstract InterceptorDAO interceptordao();

    public abstract SMSDAO smsdao();

    private static volatile AppDatabase INSTANCE;

    public static AppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            synchronized (AppDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context,
                            AppDatabase.class, Constants.SMSTABLENAME).build();
                }
            }
        }
        return INSTANCE;
    }
}
