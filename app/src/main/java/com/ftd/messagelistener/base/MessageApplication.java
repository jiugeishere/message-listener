package com.ftd.messagelistener.base;

import android.app.Application;
import android.content.ComponentCallbacks;
import android.content.ComponentName;
import android.content.Intent;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.room.Room;

import com.ftd.messagelistener.data.AppDatabase;

/**
 * tangxianfeng
 * 2021.11.28
 */
public class MessageApplication extends Application {
    private static final String TAG="MessageApplication";

    @Override
    public void onCreate() {
        Log.e(TAG, "onCreate");
        AppDatabase db = Room.databaseBuilder(getApplicationContext(),
                AppDatabase.class, "sms_message").build();
        super.onCreate();
    }

    @Override
    public void unregisterComponentCallbacks(ComponentCallbacks callback) {
        Log.e(TAG, "unregisterComponentCallbacks");
        super.unregisterComponentCallbacks(callback);
    }

    @Nullable
    @Override
    public ComponentName startForegroundService(Intent service) {
        Log.e(TAG, "startForegroundService");
        return super.startForegroundService(service);
    }

}
