package com.ftd.messagelistener.base;

import java.io.File;

public class Constants {
    public static final String SMSAction="com.ui.messagelistener.getmessage";
    public static final String SMSUpdataAction="com.ui.messagelistener.updatamessage";
    public static final String SMSTABLENAME="sms_message";

    public static final String FONT_COLOCK = "fonts" + File.separator
            + "yingpingnan.ttf";

}
