package com.ftd.messagelistener.adapter;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;
/**
 * tangxianfeng
 * 2021.11.28
 */
public class CustomFragmentAdapter extends FragmentPagerAdapter {

    private List<Fragment> fragmentList = new ArrayList<>();
    private List<String> stringList = new ArrayList<>();


    public CustomFragmentAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }


    public CustomFragmentAdapter(@NonNull FragmentManager fm, int behavior, List<Fragment> list) {
        super(fm, behavior);
        fragmentList=list;
    }

    public CustomFragmentAdapter(@NonNull FragmentManager fm, int behavior, List<Fragment> list, List<String> strings) {
        super(fm, behavior);
        fragmentList=list;
        stringList=strings;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        if (stringList.size()>0){
            return stringList.get(position);
        }
        return super.getPageTitle(position);
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }
}
