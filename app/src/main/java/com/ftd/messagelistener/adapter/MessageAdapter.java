package com.ftd.messagelistener.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.bean.SMSBean;
import com.ftd.messagelistener.utils.StringUtils;

import java.util.List;

public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.MessageViewHolder> {
    private final Context mContext;
    private List<SMSBean> smsBeans;

    public MessageAdapter(Context context,List<SMSBean> list) {
        this.mContext=context;
        this.smsBeans=list;
    }

    public void NotifyData(List<SMSBean> list){
        smsBeans=list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MessageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_message, parent, false);
        return new MessageViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessageViewHolder holder, int position) {
        if (!StringUtils.isEmpty(smsBeans.get(position).getPhone())){
            holder.messagefromText.setText(Html.fromHtml(smsBeans.get(position).getPhone()));
        }
        if (!StringUtils.isEmpty(smsBeans.get(position).getMessage())){
            holder.messagebodyText.setText(Html.fromHtml(smsBeans.get(position).getMessage()));
        }
        holder.messagedateText.setText(smsBeans.get(position).getDate());
    }



    @Override
    public int getItemCount() {
        return smsBeans.size();
    }

    public static class MessageViewHolder extends RecyclerView.ViewHolder {

         TextView messagefromText;//发消息人
         TextView messagebodyText;//消息内容
         TextView messagedateText;//消息日期

        public MessageViewHolder(@NonNull View itemView) {
            super(itemView);
            messagefromText=itemView.findViewById(R.id.message_from);
            messagebodyText=itemView.findViewById(R.id.message_body);
            messagedateText=itemView.findViewById(R.id.message_date);
        }


    }
}
