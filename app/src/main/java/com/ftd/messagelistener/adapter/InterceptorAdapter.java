package com.ftd.messagelistener.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.recyclerview.widget.RecyclerView;

import com.ftd.messagelistener.R;
import com.ftd.messagelistener.bean.InterceptorBean;
import com.ftd.messagelistener.data.AppDatabase;
import com.ftd.messagelistener.utils.StringUtils;
import com.ftd.messagelistener.utils.view.EditInterceptorDialog;

import java.util.List;
import java.util.concurrent.Executors;


public class InterceptorAdapter extends RecyclerView.Adapter<InterceptorAdapter.InterceptorViewHolder> {
    private final Context mContext;
    private List<InterceptorBean> interceptorBeans;

    public InterceptorAdapter(Context context, List<InterceptorBean> list) {
        this.mContext = context;
        this.interceptorBeans = list;
    }

    public void NotifyData(List<InterceptorBean> list) {
        interceptorBeans = list;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public InterceptorViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_interceptor, parent, false);
        return new InterceptorViewHolder(view);
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public void onBindViewHolder(@NonNull InterceptorViewHolder holder, int position) {
        holder.setIsRecyclable(false);
        holder.interceptoConfigName.setText(interceptorBeans.get(position).getConfigname());
        if (!StringUtils.isEmpty(interceptorBeans.get(position).getInterceptorcontent())){
            holder.interceptorKeyWord.setText(interceptorBeans.get(position).getInterceptorcontent());
        }else {
            holder.interceptorKeyWord.setText("不对关键词有限制");
        }

        if (!StringUtils.isEmpty(interceptorBeans.get(position).getInterceptorsend())){
            holder.interceptoSend.setText(interceptorBeans.get(position).getInterceptorsend());
        }else {
            holder.interceptoSend.setText("不对号码有限制");
        }
        holder.interceptorRelaSee.setOnLongClickListener(view -> {
            holder.interceptorRelaSee.setVisibility(View.GONE);
            holder.interceptorRelaedit.setVisibility(View.VISIBLE);
            return true;
        });
        holder.interceptorEdit.setOnClickListener(view -> {
            EditInterceptorDialog editInterceptorDialog=new EditInterceptorDialog(mContext,interceptorBeans.get(position),1);
            editInterceptorDialog.show();
        });
        holder.interceptorSee.setOnClickListener(view -> {
           EditInterceptorDialog editInterceptorDialog=new EditInterceptorDialog(mContext,interceptorBeans.get(position),2);
           editInterceptorDialog.show();
        });
        holder.interceptorDelete.setOnClickListener(view -> Executors.newSingleThreadExecutor().execute(() -> AppDatabase.getInstance(mContext).interceptordao().delete(interceptorBeans.get(position))));
        holder.interceptorRelaedit.setOnTouchListener((view, motionEvent) -> {
            Log.e("TEST","interceptorRelaedit"+view.getTag());
                holder.interceptorRelaSee.setVisibility(View.VISIBLE);
                holder.interceptorRelaedit.setVisibility(View.GONE);
                return true;
        });

    }


    @Override
    public int getItemCount() {
        return interceptorBeans.size();
    }

    public static class InterceptorViewHolder extends RecyclerView.ViewHolder {

        TextView interceptoConfigName;//配置名
        TextView interceptorKeyWord;//关键词
        TextView interceptoSend;//发送人
        AppCompatImageView interceptorSee;//放回
        AppCompatImageView interceptorEdit;//编辑
        AppCompatImageView interceptorDelete;//删除
        RelativeLayout interceptorRelaedit;//编辑图
        RelativeLayout interceptorRelaSee;//查看图

        public InterceptorViewHolder(@NonNull View itemView) {
            super(itemView);
            interceptoConfigName = itemView.findViewById(R.id.interceptor_item_configname);
            interceptorKeyWord = itemView.findViewById(R.id.interceptor_item_keyword);
            interceptoSend = itemView.findViewById(R.id.interceptor_item_send);
            interceptorSee=itemView.findViewById(R.id.interceptor_item_see);
            interceptorEdit=itemView.findViewById(R.id.interceptor_item_edit);
            interceptorDelete=itemView.findViewById(R.id.interceptor_item_delete);
            interceptorRelaedit=itemView.findViewById(R.id.interceptor_item_rela_edit);
            interceptorRelaSee=itemView.findViewById(R.id.interceptor_item_rela_see);
            interceptorDelete.setTag("item_delete");
            interceptorSee.setTag("item_see");
            interceptorEdit.setTag("item_edit");
        }


    }
}
